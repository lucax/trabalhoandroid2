package com.example.matemticadivertida;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListCell extends ArrayAdapter<String> {
    private final Activity context;
    private final Integer[] resposta;
    private final Integer[] imageId;

    public ListCell(Activity context, Integer[] resposta, Integer [] imageId){
        super(context, R.layout.list_cell, resposta);
        this.context = context;
        this.resposta = resposta;
        this.imageId = imageId;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_cell, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        ImageView imageView = (ImageView)rowView.findViewById(R.id.img);
        txtTitle.setText(resposta[position]);
        imageView.setImageResource(imageId[position]);
        return rowView;
    }
}
