package com.example.matemticadivertida;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MaiorN extends AppCompatActivity {

    TextView op1 , op2, sinal;
    EditText jog;
    int result, vljog, num1, num2, acertos=0, rounds=0, total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arit);

        op1 = findViewById(R.id.tvOp1);
        op2 = findViewById(R.id.tvOp2);
        sinal = findViewById(R.id.tvSinal);
        jog = findViewById(R.id.edJogada);

        int num1 = new Random().nextInt(10);
        int num2 = new Random().nextInt(10);
        int snl = new Random().nextInt(10);
        op1.setText(String.valueOf(num1));
        op2.setText(String.valueOf(num2));
        sinal.setText(String.valueOf(snl));

        if (num1 > num2 && num1 > snl) {
            if (num2 > snl) {
                result = (num1 * 100) + (num2 * 10) + snl;
            } else {
                result = (num1 * 100) + (snl * 10) + num2;
            }
        }
        if (num2 > num1 && num2 > snl){
            if (num1 > snl){
                result = (num2 * 100) + (num1 * 10) + snl;
            }
            else {
                result = (num2 * 100) + (snl * 10) + num1;
            }
        }
        if (snl > num1 && snl > num2)
            if (num1 > num2){
                result = (snl * 100) + (num1 * 10) + num2;
            }
            else {
                result = (snl * 100) + (num2 * 10) + num1;
         }
    }




    public void jogada(View view){
        if(jog.getText().toString().trim().length() == 0){
            Toast.makeText(this, "Campo vazio.",Toast.LENGTH_SHORT).show();
        } else {
            int vljog = Integer.parseInt(jog.getText().toString());

            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Resultado");
            if (result == vljog) {
                dialog.setMessage("resposta correta");
                acertos++;
            } else {
                dialog.setMessage("Resposta errada, correto é: " + result);
            }
            dialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.create();
            dialog.show();

            int num1 = new Random().nextInt(10);
            int num2 = new Random().nextInt(10);
            int snl = new Random().nextInt(10);
            op1.setText(String.valueOf(num1));
            op2.setText(String.valueOf(num2));
            sinal.setText(String.valueOf(snl));

            if (num1 > num2 && num1 > snl) {
                if (num2 > snl) {
                    result = (num1 * 100) + (num2 * 10) + snl;
                } else {
                    result = (num1 * 100) + (snl * 10) + num2;
                }
            }
            if (num2 > num1 && num2 > snl){
                if (num1 > snl){
                    result = (num2 * 100) + (num1 * 10) + snl;
                }
                else {
                    result = (num2 * 100) + (snl * 10) + num1;
                }
            }
            if (snl > num1 && snl > num2)
            if (num1 > num2){
                result = (snl * 100) + (num1 * 10) + num2;
            }
            else {
                result = (snl * 100) + (num2 * 10) + num1;
            }

            jog.setText("");
            rounds++;
            if(rounds>=5){
                total=acertos*20;
                Intent it = new Intent(this, TelaResult.class);
                it.putExtra("msgA", total);
                it.putExtra("msgB", "do Maior número");
                startActivity(it);
            }
        }
    }

}
