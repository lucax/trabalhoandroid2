package com.example.matemticadivertida;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class TelaResult extends AppCompatActivity {
    TextView tvRes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_result);

        tvRes = findViewById(R.id.tvRes);

        Bundle dados = getIntent().getExtras();
        int total = dados.getInt("msgA");
        String nomeJ = dados.getString("msgB");
        if (total > 50) {
            tvRes.setText("Parabens! Voce fez " + total + "pontos no jogo " + nomeJ +"!!!");
        }else{
            tvRes.setText("Voce fez " + total + " pontos no jogo " + nomeJ +". Continue tentando.");
        }
    }
    public void onClick(View view){
        Intent it = new Intent(this, MainActivity.class);
        startActivity(it);
        finish();
    }
}

