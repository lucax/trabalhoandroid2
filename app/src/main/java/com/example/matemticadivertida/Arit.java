package com.example.matemticadivertida;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Arit extends AppCompatActivity {

    TextView op1 , op2, sinal;
    EditText jog;
    int result, vljog, num1, num2, acertos=0, rounds=0, total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arit);

        op1 = findViewById(R.id.tvOp1);
        op2 = findViewById(R.id.tvOp2);
        sinal = findViewById(R.id.tvSinal);
        jog = findViewById(R.id.edJogada);

        int num1 = new Random().nextInt(10);
        int num2 = new Random().nextInt(10);
        int snl = new Random().nextInt(1);
        op1.setText(String.valueOf(num1));
        op2.setText(String.valueOf(num2));
        if (snl == 0)
            sinal.setText("-");
        else
            sinal.setText("+");
        if (snl == 0) {
            result = num1 - num2;
        } else {
            result = num1 + num2;
        }


    }

    public void valor(){

    }

    public void jogada(View view){
        if(jog.getText().toString().trim().length() == 0){
            Toast.makeText(this, "Campo vazio.",Toast.LENGTH_SHORT).show();
        } else {
            int vljog = Integer.parseInt(jog.getText().toString());

            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Resultado");
            if (result == vljog) {
                dialog.setMessage("resposta correta");
                acertos++;
            } else {
                dialog.setMessage("Resposta errada, correto é: " + result);
            }
            dialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            dialog.create();
            dialog.show();

            int num1 = new Random().nextInt(10);
            int num2 = new Random().nextInt(10);
            int snl = new Random().nextInt(1);
            op1.setText(String.valueOf(num1));
            op2.setText(String.valueOf(num2));
            if (snl == 0)
                sinal.setText("-");
            else
                sinal.setText("+");
            if (snl == 0) {
                result = num1 - num2;
            } else {
                result = num1 + num2;
            }
            jog.setText("");
            rounds++;
            if(rounds>=5){
                total=acertos*20;
                Intent it = new Intent(this, TelaResult.class);
                it.putExtra("msgA", total);
                it.putExtra("msgB", "da Aritmetica");
                startActivity(it);
            }
        }
    }

}
