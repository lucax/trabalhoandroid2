package com.example.matemticadivertida;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void contagem(View view){
        this.opcaoSelecionada(0);
    }

    public void arit(View view){
        this.opcaoSelecionada(1);
    }

    public void maiorN(View view){
        this.opcaoSelecionada(2);
    }

    public void opcaoSelecionada(int escolhaUsuario){

        if(escolhaUsuario == 0){
            Intent intent = new Intent(getApplicationContext(), Contagem.class);
            startActivity(intent);
        }

        else if (escolhaUsuario == 1){
            Intent intent = new Intent(getApplicationContext(), Arit.class);
            startActivity(intent);
        }

        else if (escolhaUsuario == 2){
            Intent intent = new Intent(getApplicationContext(), MaiorN.class);
            startActivity(intent);
        }
    }
}
